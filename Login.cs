﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace PLGSystem
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
     
        private void EnterButton_Click(object sender, EventArgs e)
        {
            string UserUsers = UsersBox.Text;

            
            if (ConfirmUsersAddress(UserUsers))
            {
                string UserPassword = PasswordBox.Text;

                if (ConfirmPassword(UserPassword))
                {
                    DataAccess _DataAccess = new DataAccess();

                    if (_DataAccess.ConfirmUser(UserUsers, UserPassword))
                    {
                        int UserID = Convert.ToInt32(_DataAccess.ReturnUserID(UserUsers));

                        Dashboard _Dashboard = new Dashboard(UserID);

                        this.Hide();

                        _Dashboard.Show();
                    }
                    else MessageBox.Show("Погрешена лозинка!");
                }
                else MessageBox.Show("Внесете лозинка!");
            }
            else MessageBox.Show("Внесете точно корисничко име!");

        }

        public bool ConfirmUsersAddress(string UserUsers)
        {
            var UsersRegex = new Regex("");
            return UsersRegex.IsMatch(UserUsers);
        }

        public bool ConfirmPassword(string UserPassword)
        {
            UserPassword = UserPassword.Trim();

            if (UserPassword == string.Empty)
            {
                return false;
            }
            else return true;
        }
        

        private void RegisterLabel_Click(object sender, EventArgs e)
        {
            Register _Register = new Register();
            this.Hide();
            _Register.Show();
        }
    }
}
